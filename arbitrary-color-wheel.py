#!/usr/bin/python3


import drawSvg as draw

def rgb2Hex(color):
    if isinstance(color,tuple):
        return '#'+''.join([format(channel, '02x') for channel in color])
    elif isinstance(color,str):
        return color

def hex2Rgb(color):
    if isinstance(color,tuple):
        return color
    elif isinstance(color,str):
        return (int(color[1:3],16),int(color[3:5],16),int(color[5:7],16))


def drawSegments(canvas, center, colors,shades):
    anglestep = 360/len(colors)
    radiusstep = 15/(2*shades+1)
    shadestep = 0.333 if shades <= 3 else 0.1
    for idx in range(len(colors)):
        for shade in range(-shades,shades+1,1):
            p = draw.Path(fill=rgb2Hex(linearShade(-shadestep*shade,colors[idx])))
            drawSegment(p,center,(15+(shade+shades)*radiusstep),(15+(shade+shades+1)*radiusstep),-anglestep/2 + anglestep*idx, -anglestep/2 + anglestep*(idx+1))
            canvas.append(p)

def drawSegment(p,center,minR,maxR,start,end):
    p.arc(center[0],center[1],maxR, start, end)
    p.arc(center[0],center[1],minR, end, start, cw=True, includeL=True)
    p.Z()
    

def linearShade(percent,color):
    sign = percent < 0
    t = 0 if sign else 255*percent
    percentChange = (1+percent) if sign else (1-percent)
    result = tuple([round(elem * percentChange + t) for elem in color])
    return result

def logShade(percent,color):
    sign = percent < 0
    t = 0 if sign else percent*255**2
    percentChange = (1+percent) if sign else (1-percent)
    [print((percentChange*elem**2 + t)**0.5) for elem in color]
    result = tuple([round((percentChange*elem**2 + t)**0.5) for elem in color])
    return result

def generateColors(colorseeds, steps):
    result = []
    for idx in range(0, len(colorseeds), 1):
        result.extend([interpolateColors(colorseeds[idx-1], colorseeds[idx], pct/100) for pct in range(0, 100, int(100/steps))])
    return result


def convertRGBtoHue(color):
    r,g,b = color
    r /= 255
    g /= 255
    b /= 255
    maxc = max(r,g,b)
    minc = min(r,g,b)
    c = maxc - minc
    hue = None
    if c == 0:
        hue = 0
    else:
        if maxc == r:
            segment = (g - b) / c
            shift = 0/60
            if segment < 0:
                shift = 360/60
            hue = segment + shift
        elif maxc == g:
            segment = (b - r)/c
            shift = 120/60
            hue = segment + shift
        elif maxc == b:
            segment = (r - g)/c
            shift = 240/60
            hue = segment + shift
        else:
            raise Exception("The max channel is not equal to one of the values!")
    return hue*60

def interpolateColors(color1, color2, percentage):
    r1,g1,b1 = color1
    r2,g2,b2 = color2

    r = int((1 - percentage)*r1 + r2*percentage)
    g = int((1 - percentage)*g1 + g2*percentage)
    b = int((1 - percentage)*b1 + b2*percentage)

    return (r,g,b)

def drawColorWheel(colors,step,shades):
    if shades > 10:
        print("Program only supports up to 10 shades.")
        exit(1)
    colors = sorted([hex2Rgb(color) for color in colors],key=convertRGBtoHue)
    width,length = (70, 70)
    canvas = draw.Drawing(width, length, origin=(0,0),displayInline=False)
    colorwheelcolors = generateColors(colors,step)
    drawSegments(canvas,(width/2,length/2),colorwheelcolors,shades)
    canvas.setPixelScale(8)
    canvas.savePng('colorwheel.png')

#drawColorWheel([(188, 41, 241), (220, 247, 80), (114, 245, 131)],7,2)
#drawColorWheel([(242, 172, 61), (81, 14, 241), (232, 51, 190)],7,2)
#drawColorWheel(['#dd0bf5', '#fbd501', '#0ef7dd'],7,2)
#drawColorWheel(['#f60892', '#4afa06', '#01e4f9'],10,2)
#drawColorWheel(['#ff218b', '#fed807', '#19b2fe'],7,2)
drawColorWheel([(81, 14, 241), '#fed807', '#4afa06','#ff218b', '#19b2fe' ],7,2)
